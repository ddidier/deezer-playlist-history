const request = require('request-promise');

const logger = require('./logger');
const PlaylistSnapshot = require('./PlaylistSnapshot');
const PlaylistTrack = require('./PlaylistTrack');

class DeezerPlaylistStore {

  /**
   * @param playlistId {int}
   */
  constructor(playlistId) {
    this._playlistId = playlistId;
    this._playlistDate = null;
    this._playlistPayload = null;
  }

  /**
   * @returns {Promise<void>}
   */
  async load() {
    this._playlistDate = new Date();
    this._playlistPayload = await DeezerPlaylistStore._loadPlaylistPayload(this._playlistId);
  }

  /**
   * @returns {PlaylistSnapshot}
   */
  snapshot() {
    this._checkDataHasBeenLoaded();
    return new PlaylistSnapshot(this._playlistId, this.tracks(), this._playlistDate);
  }

  /**
   * @returns {PlaylistTrack[]}
   */
  tracks() {
    this._checkDataHasBeenLoaded();
    return this._playlistPayload['data'].map(
      function (track) {
        return new PlaylistTrack(
          track['id'], track['title'],
          track['artist']['id'], track['artist']['name'],
          track['album']['id'], track['album']['title']);
      }
    );
  }

  /**
   * @param playlistId {int}
   * @param playlistMaxSize {int}
   * @returns {{json: boolean, uri: string}}
   * @private
   */
  static _loadPlaylistParameters(playlistId, playlistMaxSize = 9999) {
    return {
      uri: `https://api.deezer.com/playlist/${playlistId}/tracks?index=0&limit=${playlistMaxSize}`,
      // headers: {
      //   'Authorization': 'Bearer 1234567890a'
      // },
      json: true
    }
  }

  /**
   * @param playlistId {int}
   * @returns {Promise<{data: object[], checksum: string, total: int}>}
   * @private
   */
  static async _loadPlaylistPayload(playlistId) {
    logger.debug('Loading Deezer playlist %d', playlistId);
    const playlistPayload = await request(DeezerPlaylistStore._loadPlaylistParameters(playlistId));

    if (playlistPayload['error'] && playlistPayload['error']['code'] === 800) {
      logger.error('Deezer playlist %d does not exist', playlistId);
      throw new Error(`Playlist ${playlistId} does not exist!`);
    }

    logger.debug('Loaded Deezer playlist %d with %d tracks', playlistId, playlistPayload['total']);
    return playlistPayload;
  }

  /**
   * @throws Error if the payload has not been loaded.
   * @private
   */
  _checkDataHasBeenLoaded() {
    if (this._playlistPayload === null) {
      throw new Error('Data has not been loaded!');
    }
  }

}

module.exports = DeezerPlaylistStore;
