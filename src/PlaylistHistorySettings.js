class PlaylistHistorySettings {

  /**
   * Useful to test a development branch.
   * @returns {string}
   */
  static get STORE_PREFIX() {
    if (process.env.STORE_PREFIX) return process.env.STORE_PREFIX;
    return 'deezer-playlist-history';
  }

  /**
   * @returns {int}
   */
  static get STORE_VERSION() {
    return 1;
  }

}

module.exports = PlaylistHistorySettings;
