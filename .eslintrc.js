module.exports = {
  'env': {
    'commonjs': true,
    'es6': true,
    'jest/globals': true,
    'node': true
  },
  'extends': [
    'standard',
    'plugin:jest/recommended',
    'plugin:jest/style'
  ],
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly'
  },
  'parserOptions': {
    'ecmaVersion': 2018
  },
  'plugins': [
    'jest'
  ],
  'rules': {
    'object-curly-spacing': ['error', 'never'],
    'padded-blocks': ['error', {'classes': 'always'}],
    'semi': 'off',
    'space-before-function-paren': ['error', {
      'anonymous': 'always',
      'named': 'never',
      'asyncArrow': 'always'
    }]
  }
};
