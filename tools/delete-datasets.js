//
// Delete all the Apify datasets belonging to the specified user whose name matches the specified pattern.
//
// Arguments:
//   --userId the username of the Apify account
//   --token the token to access the Apify account
//   --regexp the pattern of the dataset names to be removed (default to '^dev--.+$')
//
// Example:
//   delete-datasets.js --userId=ddidier --token=1324567890ABCDEFGHIJKLMNOPQRSTUVWXYZ --regexp=^dev--.+$
//

const ApifyClient = require('apify-client');
const minimist = require('minimist');

const args = minimist(
  process.argv,
  {
    default: {
      regexp: '^dev--.+$'
    }
  });

const apifyClient = new ApifyClient({
  userId: args['userId'],
  token: args['token']
});

const regexp = new RegExp(args['regexp']);
console.log('Datasets of user "%s" with name matching "%s" will be deleted',
  args['userId'],
  regexp
);

apifyClient.datasets.listDatasets()
  .then((datasetsResult) => {

    const datasets = datasetsResult['items'];
    console.log('Existing datasets: %j',
      datasets.map(dataset => dataset['name'])
    );

    datasets
      .filter(dataset => dataset['name'].match(regexp))
      .forEach(dataset => {
        console.log('Deleting dataset "%s"', dataset['name']);
        apifyClient.datasets.deleteDataset({datasetId: dataset['id']});
      });

  });
