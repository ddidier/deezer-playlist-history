const fs = require('fs');
const path = require('path');
const logger = require('../src/logger');

const rootDirectory = path.join(__dirname, '..');
const apifyStorageDirectory = path.join(rootDirectory, 'apify_storage');

if (process.env.DELETE_APIFY_STORAGE === 'true' && fs.existsSync(apifyStorageDirectory)) {
  logger.info('Deleting Apify storage directory: %s', apifyStorageDirectory);
  fs.rmdirSync(apifyStorageDirectory, {recursive: true});
}

process.env.APIFY_LOCAL_STORAGE_DIR = apifyStorageDirectory;
