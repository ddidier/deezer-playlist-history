const PlaylistHistorySettings = require('../src/PlaylistHistorySettings');

const tu = require('./testUtilities');

describe('PlaylistHistorySettings', () => {

  beforeEach(async () => {
    delete process.env.STORE_PREFIX;
  });

  describe('#STORE_PREFIX', () => {

    test('return the default prefix when there is no custom one set', () => {
      expect(PlaylistHistorySettings.STORE_PREFIX).toStrictEqual('deezer-playlist-history');
    });

    test('return the custom prefix when there is a custom one set', () => {
      process.env.STORE_PREFIX = 'another-store-prefix';
      expect(PlaylistHistorySettings.STORE_PREFIX).toStrictEqual('another-store-prefix');
    });

  });

  describe('#STORE_VERSION', () => {

    test('returns 1', () => {
      expect(PlaylistHistorySettings.STORE_VERSION).toStrictEqual(1);
    });

  });

});
