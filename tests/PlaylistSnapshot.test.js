const PlaylistSnapshot = require('../src/PlaylistSnapshot');

const tu = require('./testUtilities');

describe('PlaylistSnapshot', () => {

  let playlistSnapshot;
  let actualPlaylistId;
  let actualPlaylistTracks ;
  let actualTakenAt;

  beforeEach(async () => {
    actualPlaylistId = 12345;
    actualPlaylistTracks = [
      tu.newPlaylistTrack(1, 101, 201),
      tu.newPlaylistTrack(2, 101, 202),
      tu.newPlaylistTrack(3, 102, 203)
    ];
    actualTakenAt = new Date(2000, 0, 1, 1, 2, 3, 4);
    playlistSnapshot = new PlaylistSnapshot(actualPlaylistId, actualPlaylistTracks, actualTakenAt);
  });

  test('#playlistId', () => {
    expect(playlistSnapshot.playlistId).toStrictEqual(actualPlaylistId);
  });

  test('#playlistTracks', () => {
    expect(playlistSnapshot.playlistTracks).toStrictEqual(actualPlaylistTracks);
  });

  test('#takenAt', () => {
    expect(playlistSnapshot.takenAt).toStrictEqual(actualTakenAt);
  });

  test('#tracksProperties', () => {
    expect(playlistSnapshot.tracksProperties).toStrictEqual([
      tu.newPlaylistTrackAsJSON(1, 101, 201),
      tu.newPlaylistTrackAsJSON(2, 101, 202),
      tu.newPlaylistTrackAsJSON(3, 102, 203)
    ]);
  });

  test('#tracksIds', () => {
    expect(playlistSnapshot.tracksIds).toStrictEqual([1, 2, 3]);
  });

  test('#tracksHash', () => {
    expect(playlistSnapshot.tracksHash).toStrictEqual('55b84a9d317184fe61224bfb4a060fb0');
  });

});
