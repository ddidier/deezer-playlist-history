const Apify = require('apify');

const PlaylistMailer = require('../src/PlaylistMailer');
const PlaylistSnapshot = require('../src/PlaylistSnapshot');

const tu = require('./testUtilities');

jest.mock('apify');

describe('PlaylistMailer', () => {

  let playlistTracks = [];
  let playlistMailer;

  beforeEach(() => {
    playlistMailer = new PlaylistMailer('test@example.com');

    for (let i = 0; i <= 4; i++) {
      playlistTracks[i] = tu.newPlaylistTrack(i, 100 + i, 200 + i);
    }
  });

  describe('#snapshotChanged', () => {

    test('raises an error when the playlist ID are different', async () => {
      const previousPlaylistSnapshot = new PlaylistSnapshot(12345, playlistTracks, new Date('2000-01-02T03:04:05.006Z'));
      const currentPlaylistSnapshot = new PlaylistSnapshot(98765, playlistTracks, new Date('2000-01-02T03:04:05.006Z'));

      await expect(playlistMailer.snapshotChanged(previousPlaylistSnapshot, currentPlaylistSnapshot))
        .rejects
        .toThrow('Playlists must be the same but are #12345 and #98765');
    });

    test('sends an email with the tracks differences', async () => {
      const previousPlaylistSnapshot = new PlaylistSnapshot(12345, [playlistTracks[0], playlistTracks[1], playlistTracks[2]], new Date('1999-01-02T03:04:05.006Z'));
      const currentPlaylistSnapshot = new PlaylistSnapshot(12345, [playlistTracks[0], playlistTracks[3], playlistTracks[4]], new Date('2000-01-02T03:04:05.006Z'));

      await playlistMailer.snapshotChanged(previousPlaylistSnapshot, currentPlaylistSnapshot);
      expect(Apify.call).toHaveBeenCalledWith('apify/send-mail', {
        to: 'test@example.com',
        subject: 'Your Deezer playlist #12345 has changed',
        html: `
<p>
Your Deezer playlist #12345 has changed<br>
between 1999-01-02T03:04:05.006Z<br>
and 2000-01-02T03:04:05.006Z<br>
</p>
<h2>Added tracks</h2>
<table>
  <thead>
    <tr>
      <th>Track ID</th>
      <th>Track Title</th>
      <th>Artist ID</th>
      <th>Artist Name</th>
      <th>Album ID</th>
      <th>Album Title</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>3</td>
      <td>Track 3</td>
      <td>103</td>
      <td>Artist 103</td>
      <td>203</td>
      <td>Album 203</td>
    </tr>
    <tr>
      <td>4</td>
      <td>Track 4</td>
      <td>104</td>
      <td>Artist 104</td>
      <td>204</td>
      <td>Album 204</td>
    </tr>
  </tbody>
</table>
<h2>Removed tracks</h2>
<table>
  <thead>
    <tr>
      <th>Track ID</th>
      <th>Track Title</th>
      <th>Artist ID</th>
      <th>Artist Name</th>
      <th>Album ID</th>
      <th>Album Title</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>Track 1</td>
      <td>101</td>
      <td>Artist 101</td>
      <td>201</td>
      <td>Album 201</td>
    </tr>
    <tr>
      <td>2</td>
      <td>Track 2</td>
      <td>102</td>
      <td>Artist 102</td>
      <td>202</td>
      <td>Album 202</td>
    </tr>
  </tbody>
</table>`
      });
    });

    test('sends an email with the tracks differences when there is no track added', async () => {
      const previousPlaylistSnapshot = new PlaylistSnapshot(12345, [playlistTracks[0], playlistTracks[1], playlistTracks[2]], new Date('1999-01-02T03:04:05.006Z'));
      const currentPlaylistSnapshot = new PlaylistSnapshot(12345, [playlistTracks[2]], new Date('2000-01-02T03:04:05.006Z'));

      await playlistMailer.snapshotChanged(previousPlaylistSnapshot, currentPlaylistSnapshot);
      expect(Apify.call).toHaveBeenCalledWith('apify/send-mail', {
        to: 'test@example.com',
        subject: 'Your Deezer playlist #12345 has changed',
        html: `
<p>
Your Deezer playlist #12345 has changed<br>
between 1999-01-02T03:04:05.006Z<br>
and 2000-01-02T03:04:05.006Z<br>
</p>
<h2>Added tracks</h2>
<p>None</p>
<h2>Removed tracks</h2>
<table>
  <thead>
    <tr>
      <th>Track ID</th>
      <th>Track Title</th>
      <th>Artist ID</th>
      <th>Artist Name</th>
      <th>Album ID</th>
      <th>Album Title</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>0</td>
      <td>Track 0</td>
      <td>100</td>
      <td>Artist 100</td>
      <td>200</td>
      <td>Album 200</td>
    </tr>
    <tr>
      <td>1</td>
      <td>Track 1</td>
      <td>101</td>
      <td>Artist 101</td>
      <td>201</td>
      <td>Album 201</td>
    </tr>
  </tbody>
</table>`
      });
    });

    test('sends an email with the tracks differences when there is no track removed', async () => {
      const previousPlaylistSnapshot = new PlaylistSnapshot(12345, [playlistTracks[0]], new Date('1999-01-02T03:04:05.006Z'));
      const currentPlaylistSnapshot = new PlaylistSnapshot(12345, [playlistTracks[0], playlistTracks[1], playlistTracks[2]], new Date('2000-01-02T03:04:05.006Z'));

      await playlistMailer.snapshotChanged(previousPlaylistSnapshot, currentPlaylistSnapshot);
      expect(Apify.call).toHaveBeenCalledWith('apify/send-mail', {
        to: 'test@example.com',
        subject: 'Your Deezer playlist #12345 has changed',
        html: `
<p>
Your Deezer playlist #12345 has changed<br>
between 1999-01-02T03:04:05.006Z<br>
and 2000-01-02T03:04:05.006Z<br>
</p>
<h2>Added tracks</h2>
<table>
  <thead>
    <tr>
      <th>Track ID</th>
      <th>Track Title</th>
      <th>Artist ID</th>
      <th>Artist Name</th>
      <th>Album ID</th>
      <th>Album Title</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>Track 1</td>
      <td>101</td>
      <td>Artist 101</td>
      <td>201</td>
      <td>Album 201</td>
    </tr>
    <tr>
      <td>2</td>
      <td>Track 2</td>
      <td>102</td>
      <td>Artist 102</td>
      <td>202</td>
      <td>Album 202</td>
    </tr>
  </tbody>
</table>
<h2>Removed tracks</h2>
<p>None</p>`
      });
    });

  });

});
