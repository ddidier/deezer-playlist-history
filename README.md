[![pipeline status](https://gitlab.com/ddidier/deezer-playlist-history/badges/master/pipeline.svg)](https://gitlab.com/ddidier/deezer-playlist-history/commits/master)
[![coverage report](https://gitlab.com/ddidier/deezer-playlist-history/badges/master/coverage.svg)](https://gitlab.com/ddidier/deezer-playlist-history/commits/master)

# Deezer Playlist History

Deezer had the bad habit of losing tracks in my playlists from time to time.

So I needed to take snapshots at regular intervals and to compare them.

Sources can be found at https://gitlab.com/ddidier/deezer-playlist-history.

Apify actor can be found at https://apify.com/c_inconnu/deezer-playlist-history.

## Workflow outline

A simple outline of the implemented workflow:

![Workflow outline](docs/workflow.png)

## Classes overview

An overview of the classes with simplified methods and fields (not the actual ones):

![Classes overview](docs/classes.png)

## How-to restore your tracks?

So, you have successfully saved the tracks of your playlist.
And now, for whatever reason, you want to restore them.
That was the whole point, after all.
It's a little involved, but not very difficult once you know how...

First of all, you need a [Deezer application](https://developers.deezer.com/myapps).
If you don't have one, "[create a new application](https://developers.deezer.com/myapps/create)" then fill the form.
Change the values according to your liking as they are not important:

| Key                               | Value                             |
|:----------------------------------|:----------------------------------|
| Name                              | `NDD Scripts`                     |
| Domain                            | `david.didier.name`               |
| Redirect URL after authentication | `http://david.didier.name/deezer` |
| Link to your Terms of Use         | `http://david.didier.name/deezer` |
| Description                       | `NDD Deezer Scripts`              |

Next is a description of the variables that you will be manipulating:

| Variable       | Description                                                                           |
|:---------------|:--------------------------------------------------------------------------------------|
| `access_code`  | the intermediate code required to get the OAuth access token                          |
| `access_token` | the OAuth access token granting permissions to change the playlist                    |
| `app_id`       | the ID of your Deezer application available on its page                               |
| `app_secret`   | the secret of your Deezer application available on its page                           |
| `playlist_id`  | the ID of the playlist you want to add the tracks to                                  |
| `redirect_uri` | the value of the `Redirect URL after authentication` field of your Deezer application |
| `track_ids`    | the comma separated IDs of the tracks to add                                          |

These variables will be noted `${my_variable}` or `<my_variable>`.

Replace the variables in the following URL and paste it in your browser:

`https://connect.deezer.com/oauth/auth.php?app_id=${app_id}&redirect_uri=${redirect_uri}&perms=manage_library`

This will redirect you to some URL like `https://david.didier.name/deezer?code=<access_code>`.

Note the access code and use it to get the access token:

`https://connect.deezer.com/oauth/access_token.php?app_id=${app_id}&code=${access_code}&secret=${app_secret}`

This will display something like `access_token=<access_token>&expires=3600`.

Note the access token and use it to add tracks to your playlist:

`http://api.deezer.com/playlist/${playlist_id}/tracks?access_token=${access_token}&request_method=post&songs=${track_ids}`
